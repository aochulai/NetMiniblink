﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace QQ2564874169.Miniblink
{
    public class CookieCollection
    {
        private bool _enable;

        public bool Enable
        {
            get { return _enable; }
            set
            {
                if (_enable != value)
                {
                    if (!value)
                    {
                        Clear();
                    }
                }

                _enable = value;
            }
        }

        private IMiniblink _miniblink;
        private string _file;

        internal CookieCollection(IMiniblink miniblink, string path = "cookies.dat")
        {
            _file = path;
            _enable = true;
            _miniblink = miniblink;
            _miniblink.RequestBefore += ClearCookie;
        }

        private void ClearCookie(object sender, RequestEventArgs e)
        {
            if (_enable) return;
            var url = e.Url.ToLower();
            if (url.Contains(".js") ||
                url.Contains(".css") ||
                url.Contains(".jpg") ||
                url.Contains(".jpeg") ||
                url.Contains(".png") ||
                url.Contains(".mp4") ||
                url.Contains(".ico") ||
                url.Contains(".font"))
            {
                return;
            }

            MBApi.wkePerformCookieCommand(_miniblink.MiniblinkHandle, wkeCookieCommand.ClearAllCookies);
        }

        public ReadOnlyCollection<Cookie> GetCookies(string url = null)
        {
            if (File.Exists(_file) == false)
            {
                throw new FileNotFoundException(_file);
            }

            MBApi.wkePerformCookieCommand(_miniblink.MiniblinkHandle, wkeCookieCommand.FlushCookiesToFile);
            if (url != null)
            {
                if (url.Contains(":") == false)
                {
                    url = "http://" + url;
                }
            }
            else
            {
                url = _miniblink.Url;
            }

            var uri = new Uri(url);
            var list = new List<Cookie>();
            var rows = File.ReadAllLines(_file, Encoding.UTF8);
            foreach (var row in rows)
            {
                if (row.StartsWith("# ")) continue;
                var items = row.Split('\t');
                if (items.Length != 7) continue;
                var domain = items[0].ToLower();
                var httpOnly = domain.StartsWith("#HttpOnly_", StringComparison.OrdinalIgnoreCase);
                if (httpOnly)
                {
                    domain = domain.Substring(domain.IndexOf("_", StringComparison.Ordinal) + 1).ToLower();
                }

                if (domain != uri.Host.ToLower()) continue;

                var cookie = new Cookie
                {
                    HttpOnly = httpOnly,
                    Domain = domain.TrimStart('.'),
                    Path = items[2],
                    Secure = "true".Equals(items[3], StringComparison.OrdinalIgnoreCase),
                    Discard = "0" == items[4],
                    Expires = "0" == items[4]
                        ? DateTime.Now.AddDays(1)
                        : new DateTime(1970, 1, 1).AddSeconds(long.Parse(items[4])),
                    Name = items[5].Split('=')[0],
                    Value = Utils.UrlEncode(items[6])
                };
                list.Add(cookie);
            }

            var rslist = new List<Cookie>();
            var rs = list.Where(i => uri.AbsolutePath.StartsWith(i.Path, StringComparison.OrdinalIgnoreCase))
                .GroupBy(k => k.Name, c => c);
            foreach (var g in rs)
            {
                if (g.Count() > 1)
                {
                    rslist.Add(g.OrderByDescending(c => c.Name.Length).First());
                }
                else if(g.Any())
                {
                    rslist.Add(g.First());
                }
            }

            return rslist.AsReadOnly();
        }

        private static string GetCurlCookie(Cookie cookie)
        {
            var ck = $"{cookie.Name}={cookie.Value};expires={cookie.Expires:R};domain={cookie.Domain};path={cookie.Path};";
            if (cookie.Secure)
            {
                ck += "secure;";
            }

            if (cookie.HttpOnly)
            {
                ck += "httponly;";
            }

            return ck;
        }

        public void Add(Cookie cookie)
        {
            if (cookie == null) return;
            if (string.IsNullOrEmpty(cookie.Path))
            {
                cookie.Path = "/";
            }

            if (string.IsNullOrEmpty(cookie.Domain))
            {
                cookie.Domain = new Uri(_miniblink.Url).Host;
            }
            MBApi.wkeSetCookie(_miniblink.MiniblinkHandle, GetCurlCookie(cookie));
        }

        public void Clear()
        {
            MBApi.wkePerformCookieCommand(_miniblink.MiniblinkHandle, wkeCookieCommand.ClearAllCookies);
            MBApi.wkePerformCookieCommand(_miniblink.MiniblinkHandle, wkeCookieCommand.FlushCookiesToFile);
        }

        public bool Contains(Cookie cookie)
        {
            if (cookie == null) return false;
            
            if (string.IsNullOrEmpty(cookie.Path))
            {
                cookie.Path = "/";
            }

            if (string.IsNullOrEmpty(cookie.Domain))
            {
                cookie.Domain = new Uri(_miniblink.Url).Host;
            }
            var list = GetCookies("http://" + cookie.Domain + cookie.Path);
            foreach (var item in list)
            {
                if (cookie.Name == item.Name && cookie.Value == item.Value && cookie.Path == item.Path)
                {
                    return true;
                }
            }

            return false;
        }

        public bool Remove(Cookie cookie)
        {
            if (cookie == null) return false;

            if (string.IsNullOrEmpty(cookie.Path))
            {
                cookie.Path = "/";
            }

            if (string.IsNullOrEmpty(cookie.Domain))
            {
                cookie.Domain = new Uri(_miniblink.Url).Host;
            }

            if (Contains(cookie))
            {
                cookie.Expires = DateTime.MinValue;
                var ck = GetCurlCookie(cookie);
                MBApi.wkeSetCookie(_miniblink.MiniblinkHandle, ck);
                MBApi.wkePerformCookieCommand(_miniblink.MiniblinkHandle, wkeCookieCommand.FlushCookiesToFile);
                return true;
            }

            return false;
        }
    }
}
